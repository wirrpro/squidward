import datetime
import time
from ..models import SquidFullData
from django.conf import settings
from django.db import connection, transaction


def delete_old_data():
    dt_today = datetime.datetime.now().strftime('%Y %m %d')
    ts_today = int(time.mktime(time.strptime(dt_today, '%Y %m %d')))

    delete_before = int(ts_today - settings.DB_STORE_TIME)

    with connection.cursor() as curs:
        curs.execute('''
                    DELETE FROM squidward_squidfulldata
                    WHERE datetime < %s
                    ''' % (delete_before))
        connection.commit()

