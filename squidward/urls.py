from django.urls import path, include
from . import views

urlpatterns = [
    path('',
         views.index, name='index'),

    path('auth/login/',
         views.auth_login, name='login'),

    path('auth/logout/',
         views.auth_logout, name='logout'),

    path('settings/',
         views.squidward_settings, name='settings'),

    path('reports/',
         views.report_index, name='report_index'),

    path('reports/top/week/',
         views.report_top_week, name='report_top_week'),

    path('reports/top/month/',
         views.report_top_month, name='report_top_month'),

    path('reports/manual/<int:fromdate>_<int:untildate>/',
         views.report_manual, name='report_manual'),

    path('reports/manual/<int:fromdate>_<untildate>/<str:user_ip>/',
         views.report_manual_user, name='report_manual_user'),

    path('reports/pptp/report/<int:fromdate>_<int:untildate>/<str:user>/',
         views.checkout_user_from_report, name='pptp_report'),

    path('reports/pptp/manual/',
         views.checkout_user_manually,name='pptp_report_manual'),

]