import time
import datetime

import locale
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404
from .forms import SquidReportIndex, Login
from .models import SquidFullData

locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')

# Main Page
def index(request):
    return render(request, 'index.html')


# Authentication
def auth_login(request):
    form = Login(request.POST, auto_id='id_%s')
    login = form['login']
    password = form['password']
    form_fields = { 'form': form,
        'login': login,
        'password': password
    }
    return render(request, 'auth/login.html', form_fields)


@login_required
def auth_logout(request):
    return render(request, 'auth/logout.html')

# Settings (paths)

@login_required
def squidward_settings(request):
    return render(request, 'squidward/settings.html')

# Reports
@login_required
def report_manual(request, fromdate, untildate, user_ip=None):
    """

    """
    fromdate, untildate, user_ip = int(fromdate), int(untildate) + 86400, str(user_ip)
    try:
        pptpip_sum = SquidFullData.objects.raw('''
          SELECT 1 as id, pptpip, trunc(sum(size)/1000000, 3) as sum
          FROM  squidward_squidfulldata
          WHERE datetime BETWEEN %s AND %s
          GROUP BY pptpip
          ORDER BY sum DESC LIMIT 50''' % (fromdate, untildate))
    except SquidFullData.DoesNotExist:
        raise Http404('Запись не существует')

    date_start = datetime.datetime.fromtimestamp(fromdate).strftime('%d %b %Y')
    date_stop = datetime.datetime.fromtimestamp(untildate).strftime('%d %b %Y')

    return render(request, 'report_manual.html', {'pptp_sum': pptpip_sum,
                                                  'date_start': date_start,
                                                  'date_stop': date_stop,
                                                  'fromdate': fromdate,
                                                  'untildate': untildate})






@login_required
def report_manual_user(request, fromdate, untildate, user_ip):
    """

    """
    fromdate, untildate, user_ip = int(fromdate), int(untildate), str(user_ip)
    try:
        pptpip_sum = SquidFullData.objects.raw('''
          SELECT 1 as id, 
          pptpip, resource, trunc(sum(size)/1000000, 3) as sum
          FROM  squidward_squidfulldata
          WHERE datetime BETWEEN %s AND %s AND pptpip = '%s'
          GROUP BY pptpip, resource
          ORDER BY sum DESC LIMIT 50''' % (fromdate, untildate, user_ip))

    except SquidFullData.DoesNotExist:
        raise Http404('Запись не существует')
    #locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
    date_start = datetime.datetime.fromtimestamp(fromdate).strftime('%d %b %Y')
    date_stop = datetime.datetime.fromtimestamp(untildate).strftime('%d %b %Y')

    return render(request, 'report_manual_user.html', {'pptp_sum': pptpip_sum,
                                                        'user_ip': user_ip,
                                                       'date_start': date_start,
                                                       'date_stop': date_stop})


@login_required
def report_index(request):
    """Main page for reporting cases"""
    first_date = SquidFullData.objects.raw(
        '''SELECT 1 as id, min(datetime) as first
           FROM squidward_squidfulldata
        '''
    )
    last_date = SquidFullData.objects.raw(
        '''SELECT 1 as id, max(datetime) as last
FROM squidward_squidfulldata
        '''
    )
    for raw in first_date:
        first_date = raw.first
    for raw in last_date:
        last_date = raw.last

    first_date = datetime.datetime.fromtimestamp(first_date).strftime('%d %b %Y')
    last_date = datetime.datetime.fromtimestamp(last_date).strftime('%d %b %Y')

    form = SquidReportIndex(request.POST, auto_id='id_%s')
    fromdate = form.fields['date_start']
    untildate = form.fields['date_stop']
    if request.method == 'POST':
        if form.is_valid():
            request.POST.get('')
            fromdate = request.POST.get('date_start')
            untildate = request.POST.get('date_stop')

            date_start = int(
                time.mktime(time.strptime(fromdate, "%m/%d/%Y")))
            date_stop = int(
                time.mktime(time.strptime(untildate, "%m/%d/%Y")))
            return HttpResponseRedirect(
                '/reports/manual/%s_%s/' % (date_start, date_stop)
                )
    form_fields = {'fromdate': fromdate, 'untildate': untildate, 'form': form,
                   'first_date': first_date, 'last_date': last_date}
    # first_date = {'first_date': first_date}
    # last_date = {'last_date': last_date}
    return render(request, 'report_index.html', form_fields)

@login_required
def report_top_week(request):
    """
    TOP 30 users-downloaders
    """
    today = datetime.datetime.combine(datetime.date.today(), datetime.time())
    last_mon = today - datetime.timedelta(days=today.weekday(), weeks=1)
    last_mon = int(datetime.datetime.timestamp(last_mon))
    last_sun = last_mon + 6 * 86400


    return HttpResponseRedirect(
        '/reports/manual/%s_%s/' % (last_mon, last_sun)
    )

    # return render(request, 'report_top_week.html', top_week)

@login_required
def report_top_month(request):
    """
    TOP 30 users-downloaders
    """
    today = datetime.datetime.combine(datetime.date.today(), datetime.time())
    last_mon = today - datetime.timedelta(days=today.weekday(), weeks=1)
    last_mon = int(datetime.datetime.timestamp(last_mon))
    day_month_ago = last_mon - 30 * 86400


    return HttpResponseRedirect(
        '/reports/manual/%s_%s/' % (day_month_ago, last_mon)
    )

@login_required
def checkout_user_from_report(request, fromdate, untildate, user_ip):
    """
    Only for checking out who loged in under user_ip
    """
    return render(request, 'pptp_checkout_report.html')


@login_required
def checkout_user_manually(request):
    """
    Operator input fromdate, untildate,
    user_ip manually in forms
    """
    return render(request, 'pptp_checkout_manual.html')


